# Copyright (C) 2012 The CyanogenMod Project <http://www.cyanogenmod.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)


# libexfat
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
    libexfat/cluster.c \
    libexfat/io.c \
    libexfat/log.c \
    libexfat/lookup.c \
    libexfat/mount.c \
    libexfat/node.c \
    libexfat/time.c \
    libexfat/utf.c \
    libexfat/utils.c

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/libexfat \
	bionic/libc/arch-arm/include \
	external/fuse/include
	
LOCAL_CFLAGS := \
	-D_FILE_OFFSET_BITS=64

LOCAL_MODULE := libexfat
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)


# dump
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
    dump/dumpexfat.8 \
    dump/main.c

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/libexfat \
	bionic/libc/arch-arm/include \
	external/fuse/include

LOCAL_SHARED_LIBRARIES := \
    libexfat libutils libdl

LOCAL_STATIC_LIBRARIES := \
	libfuse

LOCAL_CFLAGS := \
	-D_FILE_OFFSET_BITS=64

LOCAL_MODULE := dumpexfat
LOCAL_MODULE_TAGS := eng

include $(BUILD_EXECUTABLE)


# fsck
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
    fsck/exfatfsck.8 \
    fsck/main.c

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/libexfat \
	bionic/libc/arch-arm/include \
	external/fuse/include

LOCAL_SHARED_LIBRARIES := \
    libexfat libutils libdl

LOCAL_STATIC_LIBRARIES := \
	libfuse

LOCAL_CFLAGS := \
	-D_FILE_OFFSET_BITS=64

LOCAL_MODULE := fsck.exfat
LOCAL_MODULE_TAGS := eng

include $(BUILD_EXECUTABLE)


# mkfs.exfat
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
    mkfs/cbm.c \
    mkfs/fat.c \
    mkfs/main.c \
    mkfs/mkexfat.c \
    mkfs/mkexfatfs.8 \
    mkfs/rootdir.c \
    mkfs/uct.c \
    mkfs/uctc.c \
    mkfs/vbr.c

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/libexfat \
	bionic/libc/arch-arm/include \
	external/fuse/include

LOCAL_SHARED_LIBRARIES := \
    libexfat libutils libdl

LOCAL_STATIC_LIBRARIES := \
	libfuse

LOCAL_CFLAGS := \
	-D_FILE_OFFSET_BITS=64

LOCAL_MODULE := mkfs.exfat
LOCAL_MODULE_TAGS := eng

include $(BUILD_EXECUTABLE)


# mount.exfat
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
	fuse/main.c \
	fuse/mount.exfat-fuse.8

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/libexfat \
	bionic/libc/arch-arm/include \
	external/fuse/include

LOCAL_SHARED_LIBRARIES := \
    libexfat libutils libdl

LOCAL_STATIC_LIBRARIES := \
	libfuse

LOCAL_CFLAGS := \
	-D_FILE_OFFSET_BITS=64

LOCAL_MODULE := mount.exfat
LOCAL_MODULE_TAGS := eng

include $(BUILD_EXECUTABLE)


# label
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
    label/exfatlabel.8 \
    label/main.c

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/libexfat \
	bionic/libc/arch-arm/include \
	external/fuse/include

LOCAL_SHARED_LIBRARIES := \
    libexfat libutils libdl

LOCAL_STATIC_LIBRARIES := \
	libfuse

LOCAL_CFLAGS := \
	-D_FILE_OFFSET_BITS=64

LOCAL_MODULE := exfatlabel
LOCAL_MODULE_TAGS := eng

include $(BUILD_EXECUTABLE)
